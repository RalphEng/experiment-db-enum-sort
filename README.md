Experiment Database bases enum sorting
======================================

This experiment investigate how to sort in a database table by an enum column in an order that is defined dynamic.
- "enum column" mean that there is a column with a low number of different values - in Java typical expressed by an enum.
- "dynamic defined order" mean that the items are not sorted by the enum name or ordinal or any attribute that is available in the database. 

Of course one can load the rows from the table and then sort them in the application.
But if one use paging, then one need to sort before one can limit the result.
Therefore is one sort the rows not in the database but in the application, so one need to load them all.
And that is what one want not to do is one use paging.

Therefore a good solution need to do the sorting in the database.
But in the sort order is dynamic, then it need information that is not in the database.

Fortunately an enum does not have so much options, therefore the application can transfer the enum order to the database,
and then let the database order the rows.

# Spring Boot
This project use [Spring Boot](https://spring.io/projects/spring-boot) for easy setup, but the experiment itself does not depend on Spring Boot features.

# Lombok
This project use [Project Lombok](https://projectlombok.org/) therefore you need to have Lombok installed

## Lombok - Eclipse - Overlapping text edits bug
There is one problem with Eclipse: The cleanup setting with "Code Style / Use parentheses in expressions".
If it is enabled, then Eclipse cleanup will fail for `@Data` annotated classes:
>An unexpected exception occured while performing the refactoring.
>
>Overlappping text edits
https://bugs.eclipse.org/bugs/show_bug.cgi?id=535536