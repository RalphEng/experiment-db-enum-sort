package de.humanfork.experiment.dbenumsort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Component;

import de.humanfork.experiment.dbenumsort.domain.Issue;
import de.humanfork.experiment.dbenumsort.domain.IssueType;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Component
public class TestScenariosBuilders {

    @PersistenceContext
    private EntityManager entityManager;

    @RequiredArgsConstructor
    @Getter
    public class DynamicTestScneario {

        private final List<Issue> issues;

    }

    public DynamicTestScneario buildDynamicTestScnearioScenario(final List<IssueType> types) {
        List<Issue> issues = new ArrayList<Issue>();
        for (int i = 0; i < types.size(); i++) {
            Issue issue = new Issue("demo " + i, types.get(i));
            entityManager.persist(issue);
            issues.add(issue);
        }

        entityManager.flush();
        return new DynamicTestScneario(issues);
    }

    public DynamicTestScneario buildDynamicTestScnearioScenario(final IssueType... types) {
        return buildDynamicTestScnearioScenario(Arrays.asList(types));
    }
}
