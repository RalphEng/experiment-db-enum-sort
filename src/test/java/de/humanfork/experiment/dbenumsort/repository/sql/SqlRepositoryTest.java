package de.humanfork.experiment.dbenumsort.repository.sql;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.humanfork.experiment.dbenumsort.TestScenariosBuilders;
import de.humanfork.experiment.dbenumsort.TestScenariosBuilders.DynamicTestScneario;
import de.humanfork.experiment.dbenumsort.domain.Issue;
import de.humanfork.experiment.dbenumsort.domain.IssueType;
import de.humanfork.experiment.dbenumsort.repository.AbstractRepositoryTest;
import de.humanfork.experiment.dbenumsort.repository.IssueRepository;
import de.humanfork.experiment.dbenumsort.repository.sql.SqlRepository;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.util.Pair;

@SpringBootTest
@Transactional
public class SqlRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    private SqlRepository sqlRepository;

    @Autowired
    private TestScenariosBuilders testScenariosBuilders;

    @Override
    public IssueRepository getRepository() {
        return this.sqlRepository;
    }

    @Test
    public void testFindIssues_Debug() {

        DynamicTestScneario testScneario = testScenariosBuilders.buildDynamicTestScnearioScenario(IssueType.BUG,
                IssueType.FEATURE);

        List<Pair<Issue, Integer>> result = sqlRepository.findIssuesDebug(Arrays.asList(IssueType.values()));
        assertThat(result).extracting(Pair::getFirst).containsExactlyElementsOf(testScneario.getIssues());
        assertThat(result).extracting(Pair::getSecond).containsExactly(IssueType.BUG.ordinal(),
                IssueType.FEATURE.ordinal());
    }

}
