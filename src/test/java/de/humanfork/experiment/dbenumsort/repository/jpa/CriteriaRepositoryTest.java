package de.humanfork.experiment.dbenumsort.repository.jpa;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import de.humanfork.experiment.dbenumsort.repository.AbstractRepositoryTest;
import de.humanfork.experiment.dbenumsort.repository.jpa.CriteriaRepository;

import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Transactional
public class CriteriaRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    private CriteriaRepository criteriaRepository;

    @Override
    public CriteriaRepository getRepository() {
        return this.criteriaRepository;
    }

}
