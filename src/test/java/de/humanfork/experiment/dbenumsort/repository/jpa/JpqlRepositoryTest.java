package de.humanfork.experiment.dbenumsort.repository.jpa;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import de.humanfork.experiment.dbenumsort.repository.AbstractRepositoryTest;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Transactional
public class JpqlRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    private JpqlRepository jpqlRepository;

    @Override
    public JpqlRepository getRepository() {
        return this.jpqlRepository;
    }

}
