package de.humanfork.experiment.dbenumsort.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.humanfork.experiment.dbenumsort.TestScenariosBuilders;
import de.humanfork.experiment.dbenumsort.TestScenariosBuilders.DynamicTestScneario;
import de.humanfork.experiment.dbenumsort.domain.Issue;
import de.humanfork.experiment.dbenumsort.domain.IssueType;

@Transactional
public abstract class AbstractRepositoryTest {

    /**
     * The repository implementation under test.
     */
    public abstract IssueRepository getRepository();

    @Autowired
    private TestScenariosBuilders testScenariosBuilders;

    public List<Issue> sort(final Collection<Issue> unsorted, final List<IssueType> order) {
        List<Issue> sorted = new ArrayList<>(unsorted);
        sorted.sort(Comparator.comparing((final Issue issue) -> {
            int index = order.indexOf(issue.getType());
            if (index < 0) {
                return Integer.MAX_VALUE; //not found become last
            } else {
                return index;
            }
        }).thenComparing(Issue::getId));
        return sorted;
    }

    @Test
    public void testFindIssues_OrderedById() {
        DynamicTestScneario testScneario = this.testScenariosBuilders.buildDynamicTestScnearioScenario(IssueType.BUG,
                IssueType.FEATURE);

        List<Issue> result = getRepository().findIssues();
        assertThat(result).containsExactlyElementsOf(testScneario.getIssues());
    }

    @Test
    public void testFindIssues_OrderedByEnum() {
        DynamicTestScneario testScneario = this.testScenariosBuilders.buildDynamicTestScnearioScenario(IssueType.BUG,
                IssueType.FEATURE,
                IssueType.BUG,
                IssueType.UPDATE,
                IssueType.TASK);

        List<Issue> result = getRepository().findIssues(IssueType.orderByRelevance());
        assertThat(result).containsExactlyElementsOf(sort(testScneario.getIssues(), IssueType.orderByRelevance()));
    }
}
