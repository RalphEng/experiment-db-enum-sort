package de.humanfork.experiment.dbenumsort.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.NaturalId;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity 
@NoArgsConstructor(access = AccessLevel.PRIVATE) //works with Hibernate
@AllArgsConstructor
@ToString
@Getter
@EqualsAndHashCode(of = "businessKey")
public class Issue {

    @Id
    @GeneratedValue
    private Long id;
    
    @NotNull
    @NaturalId
    @Column(columnDefinition = "uuid")
    private UUID businessKey;
    
    @NotEmpty
    private String title;

    @NotNull
    @Enumerated(EnumType.STRING)
    private IssueType type;

    public Issue(@NotEmpty String title, @NotNull IssueType type) {    
        this.title = title;
        this.type = type;
        this.businessKey = UUID.randomUUID();
    }

    
}
