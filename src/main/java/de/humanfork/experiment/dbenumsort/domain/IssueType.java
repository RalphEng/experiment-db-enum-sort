package de.humanfork.experiment.dbenumsort.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum IssueType {

    TASK (4),
    
    BUG (1),
    
    FEATURE (3),
    
    UPDATE (2);
    
    private final int relevance;
    
    public static List<IssueType> orderByRelevance() {
        ArrayList<IssueType> relevance = new ArrayList<IssueType>(Arrays.asList(IssueType.values()));
        relevance.sort(Comparator.comparing(IssueType::getRelevance));
        return relevance;
    }
    
}
