package de.humanfork.experiment.dbenumsort.repository.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import de.humanfork.experiment.dbenumsort.domain.Issue;
import de.humanfork.experiment.dbenumsort.domain.IssueType;
import de.humanfork.experiment.dbenumsort.repository.IssueRepository;

@Repository
public class JpqlRepository implements IssueRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Issue> findIssues() {

        TypedQuery<Issue> query = entityManager.createQuery("SELECT issue FROM Issue issue ORDER BY issue.id",
                Issue.class);
        return query.getResultList();
    }

    @Override
    public List<Issue> findIssues(List<IssueType> orderSequence) {
        String paramPrefix = "typeOrder";
        TypedQuery<Issue> query = entityManager.createQuery(
                "SELECT issue FROM Issue issue ORDER BY "
                        + enumToSequenceNumberFunction("issue.type", paramPrefix, orderSequence.size()) + ", issue.id",
                Issue.class);
        applyEnumToSequenceParameter(paramPrefix, orderSequence, query);
        return query.getResultList();
    }

    
    /* 
     * The JPQL solution use parametrized queries (prepared statements), therefore a query need to "interaction points" (better name?)
     * - enumToSequenceNumberFunction provide the sql case statement
     * - applyEnumToSequenceParameter to apply the parameters to the query
     */
    
    private String enumToSequenceNumberFunction(final String fieldName, final String paramPrefix, int count) {
        StringBuilder query = new StringBuilder();
        query.append(" CASE \n");
        for (int i = 0; i < count; i++) {
            query.append("   WHEN " + fieldName + " = :" + enumParameterName(paramPrefix, i) + " THEN " + i + " \n");
        }
        query.append("   ELSE " + Integer.MAX_VALUE + " \n");
        query.append(" END");
        
        

        return query.toString();
    }

    private void applyEnumToSequenceParameter(final String paramPrefix, List<IssueType> orderSequence, Query query) {
        for (int i = 0; i < orderSequence.size(); i++) {
            query.setParameter(enumParameterName(paramPrefix, i), orderSequence.get(i));
        }
    }

    private String enumParameterName(final String paramPrefix, int index) {
        return paramPrefix + "EnumName" + index;
    }

}
