package de.humanfork.experiment.dbenumsort.repository.sql;

import java.util.List;
import java.util.UUID;

import org.springframework.data.util.Pair;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import de.humanfork.experiment.dbenumsort.domain.Issue;
import de.humanfork.experiment.dbenumsort.domain.IssueType;
import de.humanfork.experiment.dbenumsort.repository.IssueRepository;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class SqlRepository implements IssueRepository {

    //    NamedParameterJdbcTemplate
    private final JdbcTemplate jdbcTemplate;

    @Override
    public List<Issue> findIssues() {
        return jdbcTemplate.query("SELECT id, business_key, title, type FROM issue ORDER BY id", ISSUE_MAPPER);
    }

    @Override
    public List<Issue> findIssues(final List<IssueType> orderSequence) {
        return jdbcTemplate.query(
                "SELECT id, business_key, title, type FROM issue ORDER BY " + enumToSequenceNumberFunction(orderSequence) + ", id", ISSUE_MAPPER);
    }
    
    public List<Pair<Issue, Integer>> findIssuesDebug(final List<IssueType> order) {
        return jdbcTemplate.query(
                "SELECT id, business_key, title, type, " + enumToSequenceNumberFunction(order) + " AS TYPE_SEQUENCE_NUMBER FROM issue",
                (rs, rowNum) -> Pair.of(ISSUE_MAPPER.mapRow(rs, rowNum), rs.getInt("TYPE_SEQUENCE_NUMBER")));
    }


    private RowMapper<Issue> ISSUE_MAPPER = (rs, rowNum) -> new Issue(rs.getLong("id"),
            UUID.fromString(rs.getString("business_key")),
            rs.getString("title"),
            IssueType.valueOf(rs.getString("type")));

    private String enumToSequenceNumberFunction(final List<IssueType> order) {
        StringBuilder query = new StringBuilder();
        query.append(" CASE \n");
        for (int i = 0; i < order.size(); i++) {
            query.append("   WHEN type = '" + order.get(i).name() + "' THEN " + i + " \n");
        }
        query.append("   ELSE " + Integer.MAX_VALUE + " \n");
        query.append(" END");

        return query.toString();
    }

}
