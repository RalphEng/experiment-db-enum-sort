package de.humanfork.experiment.dbenumsort.repository.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.Case;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import de.humanfork.experiment.dbenumsort.domain.Issue;
import de.humanfork.experiment.dbenumsort.domain.IssueType;
import de.humanfork.experiment.dbenumsort.repository.IssueRepository;

@Repository
public class CriteriaRepository implements IssueRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Issue> findIssues() {
        CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();

        CriteriaQuery<Issue> criteriaQuery = criteriaBuilder.createQuery(Issue.class);
        Root<Issue> root = criteriaQuery.from(Issue.class);
        criteriaQuery.select(root);

        criteriaQuery.orderBy(criteriaBuilder.asc(root.get("id")));

        TypedQuery<Issue> query = entityManager.createQuery(criteriaQuery);
        return query.getResultList();
    }

    @Override
    public List<Issue> findIssues(List<IssueType> orderSequence) {
        CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();

        CriteriaQuery<Issue> criteriaQuery = criteriaBuilder.createQuery(Issue.class);
        Root<Issue> root = criteriaQuery.from(Issue.class);
        criteriaQuery.select(root);

        criteriaQuery.orderBy(
                criteriaBuilder.asc(enumToSequenceNumberFunction(root.get("type"), orderSequence, criteriaBuilder)),
                criteriaBuilder.asc(root.get("id")));

        TypedQuery<Issue> query = entityManager.createQuery(criteriaQuery);
        return query.getResultList();
    }

    private Case<Integer> enumToSequenceNumberFunction(Path<IssueType> orderedAttribute, List<IssueType> orderSequence,
            CriteriaBuilder criteriaBuilder) {
        Case<Integer> selectCase = criteriaBuilder.<Integer>selectCase();
        for (int i = 0; i < orderSequence.size(); i++) {
            selectCase.when(criteriaBuilder.equal(orderedAttribute, orderSequence.get(i)), i);
        }
        selectCase.otherwise(Integer.MAX_VALUE);
        return selectCase;
    }

}
