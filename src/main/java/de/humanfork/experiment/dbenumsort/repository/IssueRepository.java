package de.humanfork.experiment.dbenumsort.repository;

import java.util.List;

import de.humanfork.experiment.dbenumsort.domain.Issue;
import de.humanfork.experiment.dbenumsort.domain.IssueType;

public interface IssueRepository {

    List<Issue> findIssues();

    List<Issue> findIssues(List<IssueType> orderSequence);

}
