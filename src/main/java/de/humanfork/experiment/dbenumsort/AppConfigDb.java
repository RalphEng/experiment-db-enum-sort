package de.humanfork.experiment.dbenumsort;

import javax.sql.DataSource;
import org.jdbcdslog.DataSourceProxy;
import org.jdbcdslog.JDBCDSLogException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import com.zaxxer.hikari.HikariDataSource;

/**
 * Configuration for database related beans.
 *
 * <p>
 * Does not use springs boots {@link org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration}
 * but Spring boots {@link org.springframework.boot.jdbc.DataSourceBuilder} that is configured to behave like
 * {@code DataSourceAutoConfiguration}. - see {@link #hikariPoolDataSource(DataSourceProperties)}
 * </p>
 */
@Configuration
public class AppConfigDb {

    /** Logger for this class. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AppConfigDb.class);

    /**
     * Needed for {@link #hikariPoolDataSource(DataSourceProperties)} manual Datasource configuration.
     * @return the datasource configuration
     */
    @Bean
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSourceProperties dataSourceProperties() {
        return new DataSourceProperties();
    }

    /**
     * Configure HikariCP pooled DataSource, with jdbc-log proxy.
     *
     * <h2>Manual Datasource configuration</h2>
     * <p>
     * This Setup is in sync with what Spring Boot does for you by default (except that a dedicated connection pool
     * is chosen in code). See last example in "Spring Boot How to" Chapter "9.1 Configure a Custom DataSource"
     * <a href="https://docs.spring.io/spring-boot/docs/current/reference/html/howto.html#howto-configure-a-datasource">
     * https://docs.spring.io/spring-boot/docs/current/reference/html/howto.html#howto-configure-a-datasource
     * </a>
     * This setup use the normal spring boot datasource configuration properties:
     * {@code spring.datasource.*} and {@code spring.datasource.hikari.*}.
     * </p>
     *
     * <p>
     * Attention: {@code ConfigurationProperties(prefix = "spring.datasource.hikari")} on a {@code Bean} method:
     * The configuration is executed ON the result returned from the bean-method, AFTER the method returned it!
     * Therefore, {@link #hikariPoolDataSource(DataSourceProperties)} and {@link #dataSource(HikariDataSource)}
     * need to be two distinct methods in order to get the configuration applied to the {@link HikariDataSource}.
     * </p>
     *
     * <h2>jdbc-log proxy</h2>
     * <p>
     * The jdbc datasource log proxy is configured by properties from {@code jdbcdslog.properties} file and
     * logging setup for  {@link org.jdbcdslog.ConnectionLogger}, {@link org.jdbcdslog.StatementLogger},
     * {@link org.jdbcdslog.SlowQueryLogger} and {@link org.jdbcdslog.ResultSetLogger} (in {@code logback.xml})
     * </p>
     *
     * @param properties from {@link #dataSourceProperties()}
     * @see <a href="https://docs.spring.io/spring-boot/docs/current/reference/html/howto.html#howto-configure-a-datasource">
     *      Spring Boot How to / Chapter 9.1 Configure a Custom DataSource</a>
     * @return the pooled data source
     */
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.hikari")
    public HikariDataSource hikariPoolDataSource(final DataSourceProperties properties) {
        HikariDataSource hikariPoolDataSource = properties.initializeDataSourceBuilder().type(HikariDataSource.class)
                .build();

        return hikariPoolDataSource;
    }

    /**
     * Data source.
     *
     * @param hikariPoolDataSource the hikari pool data source
     * @return the data source wrapped with an logger.
     * @throws JDBCDSLogException thrown if jdbc logging proxy setup fails.
     */
    @Primary
    @Bean
    public DataSource dataSource(final HikariDataSource hikariPoolDataSource) throws JDBCDSLogException {

        /* this check can not be done this within HikariDataSource hikariPoolDataSource(DataSourcePropertie) because
         * the @ConfigurationProperties(prefix = "spring.datasource.hikari") "advice" is executed after method
         * returned its result on that result.
         */
        if ((hikariPoolDataSource.getConnectionInitSql() == null)
                || !hikariPoolDataSource.getConnectionInitSql().toUpperCase().contains("NLS_SORT")) {
            LOGGER.warn(
                    "HikariDataSource should have a connectionInitSql with `ALTER SESSION SET NLS_SORT=GERMAN` to controll the sort order, but the init sql is `{}`",
                    hikariPoolDataSource.getConnectionInitSql());
        }

        DataSourceProxy loggingDataSourceProxy = new DataSourceProxy();
        loggingDataSourceProxy.setTargetDSDirect(hikariPoolDataSource);

        return loggingDataSourceProxy;
    }   
}
