package de.humanfork.experiment.dbenumsort;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Exclude DataSourceAutoConfiguration.class because we use ower own AppConfigDb. 
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class App {
    
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    public static void main(final String[] args) {
        SpringApplication.run(App.class, args);
    }
}
